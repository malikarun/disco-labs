# frozen_string_literal: true

require 'net/http'
require 'uri'
require 'json'
require 'openssl'
require 'base64'

# App Class
class App
  FILE_PATH = './COVER_LETTER.md'
  EMAIL = 'malikarun007@gmail.com'
  SECRET = 'JDDX6jMbbsF4QhA222cwrEfr'
  API_URL = URI.parse('https://jobs.discolabs.app/api/v1/applications')
  DIGEST = OpenSSL::Digest.new('sha256')
  CV_URL = 'https://bitbucket.org/malikarun/disco-labs/src/master/'
  GITHUB_PROFILE = 'https://github.com/malikarun'
  LINKED_IN_PROFILE = 'https://www.linkedin.com/in/arun-malik-a3772954/'
  PHONE_NUMBER = '0433740350'
  ROLE_ID = 'a12a2ac2-7511-404e-8154-65e90c151a42'

  def initialize
    @disco_score = 0
    @cover_letter = ''
    @alphabets = {}
  end

  def run
    calculate_alphabet_score
    calculate_disco_score
    send_request
  end

  private

  # Setting integer value for each alpabet
  def calculate_alphabet_score
    ('a'..'z').to_a.each_with_index { |letter, i| @alphabets[letter] = i % 5 }
  end

  def calculate_disco_score
    # Read file by each line
    File.open(FILE_PATH).each_line do |line|
      # append each line to cover letter
      @cover_letter += line

      # for each letter in the line, find a corresponding integer value
      line.split('').each do |char|
        # update th score
        @disco_score += @alphabets[char.downcase].to_i
      end
    end
  end

  def signature
    Base64.encode64(OpenSSL::HMAC.digest('sha256', SECRET, payload)).strip
  end

  def header
    {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': "hmac #{EMAIL}:#{signature}"
    }
  end

  def attributes
    {
      cv_url: CV_URL,
      github_profile: GITHUB_PROFILE,
      linked_in_profile: LINKED_IN_PROFILE,
      phone_number: PHONE_NUMBER,
      role_id: ROLE_ID,
      disco_score: @disco_score,
      cover_letter: @cover_letter
    }
  end

  def payload
    {
      data: {
        type: 'application',
        attributes: attributes
      }
    }.to_json
  end

  def send_request
    http = Net::HTTP.new(API_URL.host, API_URL.port)
    http.use_ssl = true
    request = Net::HTTP::Post.new(API_URL.request_uri, header)
    request.body = payload

    # Send the request
    response = http.request(request)

    p response.read_body
  end
end
