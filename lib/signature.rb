# frozen_string_literal: true

require 'openssl'
require 'base64'
require 'json'

# Sinature Class
class Sinature
  SECRET = 'VEa1Tj1Fgcwn25TByLHjUNe1'

  def attributes
    {
      first_name: 'David',
      last_name: 'Bird',
      role: 'mid-level-developer',
      cover_letter: 'Disco is cool.',
      cv_url: 'https://example.com/cv.pdf',
      github_profile: 'https://github.com/me',
      linked_in_profile: 'https://www.linkedin.com/in/me',
      disco_score: 82
    }
  end

  def data
    {
      data: {
        type: 'application',
        attributes: attributes
      }
    }
  end

  def result
    hash = OpenSSL::HMAC.digest('sha256', SECRET, data.to_json)
    Base64.encode64(hash).strip
  end
end
