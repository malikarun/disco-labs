# frozen_string_literal: true

require 'minitest/autorun'
require_relative '../lib/signature'

describe Sinature do
  it 'should create correct signature' do
    signature = '1fyxt9EvlZ1Tu6JPSbrCHClfw0iGGOwcZh6rwM1Q+Bs='
    sign = Sinature.new
    assert_equal signature, sign.result.encode!(Encoding::UTF_8)
  end
end
