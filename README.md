# ruby_project
Toy Robot

## Install deps
`gem install bundler && bundle install`.

## Test
Run `rake test` to run the tests

## Running the project
`rake run` to run the program.

## Linting
`rake lint` to run linting

