require 'net/http'
require 'uri'
require 'json'
require 'openssl'
require 'Base64'

# Inital data
disco_score = 0
cover_letter = ''
alphabets = {}
email = 'malikarun007@gmail.com'
secret = 'JDDX6jMbbsF4QhA222cwrEfr'
uri = URI.parse("https://jobs.discolabs.app/api/v1/applications")
digest = OpenSSL::Digest.new('sha256')

# Setting integer value for each alpabet
('a'..'z').to_a.each_with_index {|letter, i| alphabets[letter] = i % 5}
file_path = './COVER_LETTER.md'

# Read file by each line
File.open(file_path).each_line do |line|
	# append each line to cover letter
	cover_letter += line

	# for each letter in the line, find a corresponding integer value
    line.split('').each do |char|
		# update th score
		disco_score += alphabets[char.downcase].to_i
	end
end

payload = {
	data: {
		type: 'application',
		attributes:  {
			cv_url: 'https://bitbucket.org/malikarun/disco-labs/src/master/',
			github_profile: 'https://github.com/malikarun',
			linked_in_profile: 'https://www.linkedin.com/in/arun-malik-a3772954/',
			phone_number: '0433740350',
			role_id: 'a12a2ac2-7511-404e-8154-65e90c151a42',
			disco_score: disco_score,
			cover_letter: cover_letter
		}
	}
}.to_json

test_data = {"data":{"type":"application","attributes":{"first_name":"David","last_name":"Bird","role":"mid-level-developer","cover_letter":"Disco is cool.","cv_url":"https://example.com/cv.pdf","github_profile":"https://github.com/me","linked_in_profile":"https://www.linkedin.com/in/me","disco_score":82}}}
test_secret = 'VEa1Tj1Fgcwn25TByLHjUNe1'
signature = OpenSSL::HMAC.hexdigest(digest, test_secret, Base64.encode64(test_data.to_json))

p signature

# # signature = Base64.encode64(OpenSSL::HMAC.hexdigest(digest, secret, payload))
# # signature = OpenSSL::HMAC.hexdigest(digest, secret, payload)
# # signature = OpenSSL::HMAC.hexdigest('SHA256', secret, payload)
# signature = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha256'), secret, payload)

# header = {
# 	'Accept': 'application/json',
# 	'Content-Type': 'application/json',
# 	'Authorization': "hmac #{email}:#{signature}"
# }

# # Create the HTTP objects
# http = Net::HTTP.new(uri.host, uri.port)
# http.use_ssl = true
# request = Net::HTTP::Post.new(uri.request_uri, header)
# request.body = payload

# # # Send the request
# response = http.request(request)

# p response