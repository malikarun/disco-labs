# <p style="text-align: center">Mr. Arun Kumar </p>

##### 0433740350
##### malikarun@gmail.com
##### 13 Robusta Place, Forest Lake, QLD
##### Sr. Software Engineer Position

<br/>

### About me

I am a software engineer with wide variety of experience in frontend, backend and devops areas. At my first job in march 2013, I started with ruby as my first full stack language and fell in love with it. I mainly used Ruby with Rails and Sinatra. I used jQuery and bootstrap with Ruby on Rails. I also started moving to Angular and then React world on the front and while keeping Ruby as the backend api language. We moved from Rails 2 to 3, 4 and then 5.

After almost 6 years, I changed jobs and went for a frontend position. I worked with Angular and React and along with that I started looking into devops. I learned at automatic CI/CD with jenkins and Bamboo, Infrastructure as code for aws, docker and ecs deployments for micro services and serverless architecture.

### Why I am excited about disco

I came across Senior Software Engineer role at disco. The role excited me for several reasons. I love Ruby and Javascript and disco is doing both. I am very excited about GraphQL. I want to go to a position where I can mentor my colleagues at the same time when I can still code in my favorite languages. I am excited about my career progress and what opportunity it brings to me and how I can utilize my skills for the disco labs.

### My recent work

I have recently working as part of Research and Development team at one of the fastest growing insurance company in Australia. My team focuses on process and architecture improvements. We come up with a solution with current tech stack, do a proof of concept and then transfer the knowledge to other teams. We spend a lot of time coordinating with other developers about how to improve for future. This process always helps me to be better at communicating changes, doing presentations and mentor others to follow the guidelines.

With my breadth of experience I believe that I would make an excellent addition to your team.

Sincerely,

Arun Kumar